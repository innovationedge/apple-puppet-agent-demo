#!/bin/bash

CONFIG_PATH=/vagrant/bootstrap/config/$1
echo 'Configuring hosts.'
sudo hostname $1
yes | sudo cp $CONFIG_PATH/hosts /etc/hosts
echo 'Configuring DNS.'
yes | sudo cp $CONFIG_PATH/resolv.conf /etc/resolv.conf
echo 'Configuring proxy.'
export HTTP_PROXY="http://proxy-wsa.esl.cisco.com:80/"
export HTTPS_PROXY="https://proxy-wsa.esl.cisco.com:80/"
export http_proxy=$HTTP_PROXY
export https_proxy=$HTTPS_PROXY
printf "export HTTP_PROXY=\"$HTTP_PROXY\"\nexport HTTPS_PROXY=\"$HTTPS_PROXY\"\nexport http_proxy=\"$HTTP_PROXY\"\nexport https_proxy=\"$HTTPS_PROXY\"" | sudo tee /etc/profile.d/cisco_proxy.sh
echo 'Configuring syslog server to receive container messages over TCP.'
RSYSLOG_CONF_DIR="/etc/rsyslog.d/"
RSYSLOG_CONF_FILE="50-default.conf"
sudo cp $RSYSLOG_CONF_DIR/$RSYSLOG_CONF_FILE /tmp/$RSYSLOG_CONF_FILE
yes | sudo cp $CONFIG_PATH/rsyslog.conf $RSYSLOG_CONF_DIR/$RSYSLOG_CONF_FILE
cat /tmp/$RSYSLOG_CONF_FILE | sudo tee -a $RSYSLOG_CONF_DIR/$RSYSLOG_CONF_FILE
sudo service rsyslog restart
echo 'Setting up Puppet Master.'
wget -q http://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
sudo dpkg -i puppetlabs-release-pc1-trusty.deb
sudo apt-get update
sudo apt-get --yes --force-yes install build-essential git puppetserver
echo 'Copying Puppet config file.'
yes | sudo cp $CONFIG_PATH/puppet.conf /etc/puppetlabs/puppet/puppet.conf
echo 'Installing Cisco YANG module for Puppet.'
git clone http://github.com/cisco/cisco-yang-puppet-module.git
cd cisco-yang-puppet-module
/opt/puppetlabs/puppet/bin/puppet module build
sudo --preserve-env /opt/puppetlabs/puppet/bin/puppet module install pkg/*.tar.gz
echo 'Starting Puppet.'
#sudo /opt/puppetlabs/bin/puppet resource service puppetserver ensure=running enable=true
sudo service puppetserver start
