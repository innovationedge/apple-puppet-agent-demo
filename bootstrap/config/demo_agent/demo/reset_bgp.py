import ConfigParser
from ios_xr_grpc_python.lib.cisco_grpc_client import CiscoGRPCClient

def main():
		config = ConfigParser.RawConfigParser()
		config.read('demo.config')
		ip = config.get('demo_agent', 'ip')
        bgp_config = open('base_bgp.json', 'r').read()
        client = CiscoGRPCClient(ip, 57777, 10, 'vagrant', 'vagrant', None, None)
        client.replaceconfig(bgp_config)
        print 'Reset BGP configs.'

if __name__ == '__main__':
        main()