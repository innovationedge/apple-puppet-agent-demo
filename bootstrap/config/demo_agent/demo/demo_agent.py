import os
import time
import syslog
import ConfigParser
from ios_xr_grpc_python.lib.cisco_grpc_client import CiscoGRPCClient

def main():
	#creds = open('ems.pem').read()
	#options='ems.cisco.com'
	#client = CiscoGRPCClient('11.1.1.10', 57777, 10, 'vagrant', 'vagrant', creds, options)
	config = ConfigParser.RawConfigParser()
	config.read('demo.config')
	ip = config.get('demo_agent', 'ip')
	syslog.openlog(facility=syslog.LOG_DAEMON)
	syslog.syslog(syslog.LOG_NOTICE, str(ip) + ': Starting demo agent daemon.')
	client = CiscoGRPCClient(ip, 57777, 10, 'vagrant', 'vagrant', None, None)
	bgp_yang_path = '{"Cisco-IOS-XR-ipv4-bgp-cfg:bgp": [null]}'
	while True:
		while os.path.isfile('/home/ubuntu/new_bgp.json') is False:
			time.sleep(3)
		orig_bgp_config = client.getconfig(bgp_yang_path)
		open('orig_bgp.json', 'w').write(orig_bgp_config)
		syslog.syslog(syslog.LOG_INFO, 'Retrieved current config to: orig_bgp.json')
		new_bgp_config = open('/home/ubuntu/new_bgp.json', 'r').read()
		replace_return = client.replaceconfig(new_bgp_config)
		open('replace_return.json', 'w').write(str(replace_return))
		curr_bgp_config = client.getconfig(bgp_yang_path)
		open('curr_bgp.json', 'w').write(curr_bgp_config)
		syslog.syslog(syslog.LOG_INFO, 'New BGP config retrieved to: curr_bgp.json')
		syslog.syslog(syslog.LOG_NOTICE, str(ip) + ': Changes processed, deleting supplied config record.')
		os.remove('/home/ubuntu/new_bgp.json')
	syslog.closelog()

if __name__ == '__main__':
	main()