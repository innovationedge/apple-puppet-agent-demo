#!/bin/bash

cd ~
# rm new_bgp.conf
python reset_bgp.py
python single_comm.py "show bgp neighbors" > orig_bgp_neighbors.txt
# separately
python demo_agent.py &
# original
puppet agent -t
python single_comm.py "show bgp neighbors" > curr_bgp_neighbors.txt
bash bgp_stats.sh orig_bgp_neighbors.txt && bash gbp_stats.sh curr_bgp_neighbors.txt
# or
grep -e connection-established-time -e neighbor-address orig_bgp_neighbors.txt
grep -e connection-established-time -e neighbor-address curr_bgp_neighbors.txt