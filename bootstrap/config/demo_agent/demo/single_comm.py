import sys
import ConfigParser
from ios_xr_grpc_python.lib.cisco_grpc_client import CiscoGRPCClient

def main():
		config = ConfigParser.RawConfigParser()
		config.read('demo.config')
		ip = config.get('demo_agent', 'ip')
        client = CiscoGRPCClient(ip, 57777, 10, 'vagrant', 'vagrant', None, None)
        output = client.showcmdjsonoutput(sys.argv[1])
        print output

if __name__ == '__main__':
        main()