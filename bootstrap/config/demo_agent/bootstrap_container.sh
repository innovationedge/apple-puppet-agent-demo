#!/bin/bash
#Only necessary if straight cp to rootfs
#echo 'Owning home.'
#sudo chown -R ubuntu:ubuntu /home/ubuntu/
echo 'Configuring syslog.'
SYSLOG_DEST="10.1.1.50:10514"
printf "daemon.notice	@@$SYSLOG_DEST" | sudo tee -a /etc/rsyslog.d/50-default.conf
sudo service rsyslog restart
echo 'Configuring demo agent daemon.'
yes | sudo cp ~/demo_agent /etc/init.d/demo_agent
sudo chmod +x /etc/init.d/demo_agent
sudo update-rc.d demo_agent defaults
echo 'Supplying demo agent config.'
printf "[demo_agent]\nip=$1\n" > /home/ubuntu/demo/demo.config
echo 'Starting demo agent daemon.'
sudo service demo_agent start