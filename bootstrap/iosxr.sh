#!/bin/bash

echo "Setting hostname to $1."
sudo hostname $1
echo "Setting lab-specific config."
export HTTP_PROXY="http://proxy-wsa.esl.cisco.com:80/"
export HTTPS_PROXY="https://proxy-wsa.esl.cisco.com:80/"
export http_proxy=$HTTP_PROXY
export https_proxy=$HTTPS_PROXY
printf "nameserver 64.102.6.247\nnameserver 72.168.47.11\nnameserver 173.36.131.10\ndomain cisco.com\nsearch cisco.com" | sudo tee -a /etc/resolv.conf
echo "Configuring Puppet for XR."
sudo --preserve-env rpm --import http://yum.puppetlabs.com/RPM-GPG-KEY-puppetlabs
sudo --preserve-env rpm --import http://yum.puppetlabs.com/RPM-GPG-KEY-reductive
wget -q https://yum.puppetlabs.com/puppetlabs-release-pc1-cisco-wrlinux-7.noarch.rpm
sudo --preserve-env yum install -y puppetlabs-release-pc1-cisco-wrlinux-7.noarch.rpm
sudo --preserve-env yum update -y
echo "Installing Puppet."
sudo --preserve-env yum install -y puppet
echo "Configuring Puppet."
printf "127.0.0.1 $1  $1.cisco.com\n10.1.1.50 puppet puppet.cisco.com" | sudo tee -a /etc/hosts
printf "[main]\n server = puppet" | sudo tee -a /etc/puppetlabs/puppet/puppet.conf
echo "Installing gRPC for Puppet."
export PATH=/opt/puppetlabs/puppet/bin:$PATH
wget -q https://rubygems.org/downloads/grpc-0.15.0-x86_64-linux.gem
sudo --preserve-env /opt/puppetlabs/puppet/bin/gem install --http-proxy http://proxy-wsa.esl.cisco.com:80/ --no-rdoc --no-ri grpc
sudo mv /home/vagrant/cisco_yang.yaml /etc/cisco_yang.yaml
echo "Configuring demo container."
sudo cp /home/vagrant/demo_agent/apple-agent-demo-rootfs.tar.gz /misc/app_host/
cd /misc/app_host/
sudo mkdir apple-agent-demo
tar -zvxf apple-agent-demo-rootfs.tar.gz -C apple-agent-demo/ &> /dev/null
echo "Launching demo container."
sudo -i virsh create /home/vagrant/demo_agent/apple-agent-demo.xml
echo "Sleeping 5 minutes while it comes up."
sleep 5m
echo "Copying demo configs to container."
sshpass -pubuntu scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=3" -P 58822 -r /home/vagrant/demo_agent/demo/* ubuntu@127.0.0.1:/home/ubuntu/demo/
#sudo cp /home/vagrant/demo_agent/demo/* /misc/app_host/apple-agent-demo/home/ubuntu/demo/
sshpass -pubuntu scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=3" -P 58822 /home/vagrant/demo_agent/bootstrap_container.sh ubuntu@127.0.0.1:/home/ubuntu/bootstrap_container.sh
#sudo cp /home/vagrant/demo_agent/bootstrap_container.sh /misc/app_host/apple-agent-demo/home/ubuntu/
sshpass -pubuntu scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=3" -P 58822 /home/vagrant/demo_agent/demo_agent ubuntu@127.0.0.1:/home/ubuntu/demo_agent
#sudo cp /home/vagrant/demo_agent/demo_agent /misc/app_host/apple-agent-demo/home/ubuntu/demo_agent
echo "Running container bootstrap."
sshpass -pubuntu ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=3" -E "install_agent.log" -p 58822 -tt ubuntu@127.0.0.1 "sudo bash /home/ubuntu/bootstrap_container.sh $2"
echo "Applying base networking config."
source /pkg/bin/ztp_helper.sh

function configure_xr() 
{
   ## Apply a blind config 
   xrapply $1
   if [ $? -ne 0 ]; then
       echo "xrapply failed to run"
   fi
   xrcmd "show config failed" > /home/vagrant/config_failed_check
}

cd /home/vagrant/
config_file=$1
configure_xr $config_file

cat /home/vagrant/config_failed_check
grep -q "ERROR" /home/vagrant/config_failed_check
 
if [ $? -ne 0 ]; then
    echo "Configuration was successful!"
    echo "Last applied configuration was:"
    xrcmd "show configuration commit changes last 1"
else
    echo "Configuration Failed. Check /home/vagrant/config_failed on the router for logs"
    xrcmd "show configuration failed" > /home/vagrant/config_failed
    exit 1
fi
echo "Finished XR bootstrap."
