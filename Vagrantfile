# -*- mode: ruby -*-
# vi: set ft=ruby :
# Remington Campbell <remcampb@cisco.com>

# Add shared folder pathing here.
# host_src is a relative path.
# guest_dest MUST be an absolute path on the guest. Does not need to exist.
shares = [
]

# IOS-XR 
# hostname is name of box
# links contains used link params
# ports contains port params, objects are {:guest=>?, :host=>?}
# ram is ram for machine
iosxrs = [
  {
    :box => "IOS XRv",
    :hostname => "rtr1",
    :links => [{
        :network => "private_network",
        :vbox_intnet => "mgmt",
        #ip is not applied right now, no auto config
        :ip => "10.1.1.1",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link1",
        #ip is not applied right now, no auto config
        :ip => "11.1.1.1",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link2",
        #ip is not applied right now, no auto config
        :ip => "12.1.1.1",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link3",
        #ip is not applied right now, no auto config
        :ip => "13.1.1.1",
        :auto_config => false
    }],
    :ports => [],
    :ram => 4096,
    :provision_script => "bootstrap/iosxr.sh"
  },
  {
    :box => "IOS XRv",
    :hostname => "rtr2",
    :links => [{
        :network => "private_network",
        :vbox_intnet => "mgmt",
        #ip is not applied right now, no auto config
        :ip => "10.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link1",
        #ip is not applied right now, no auto config
        :ip => "11.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link4",
        #ip is not applied right now, no auto config
        :ip => "14.1.1.1",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link7",
        #ip is not applied right now, no auto config
        :ip => "17.1.1.1",
        :auto_config => false
    }],
    :ports => [],
    :ram => 4096,
    :provision_script => "bootstrap/iosxr.sh"
  },
  {
    :box => "IOS XRv",
    :hostname => "rtr3",
    :links => [{
        :network => "private_network",
        :vbox_intnet => "mgmt",
        #ip is not applied right now, no auto config
        :ip => "10.1.1.3",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link3",
        #ip is not applied right now, no auto config
        :ip => "13.1.1.1",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link4",
        #ip is not applied right now, no auto config
        :ip => "14.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link5",
        #ip is not applied right now, no auto config
        :ip => "15.1.1.1",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link6",
        #ip is not applied right now, no auto config
        :ip => "16.1.1.1",
        :auto_config => false
    }],
    :ports => [],
    :ram => 4096,
    :provision_script => "bootstrap/iosxr.sh"
  },
  {
    :box => "IOS XRv",
    :hostname => "rtr4",
    :links => [{
        :network => "private_network",
        :vbox_intnet => "mgmt",
        #ip is not applied right now, no auto config
        :ip => "10.1.1.4",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link2",
        #ip is not applied right now, no auto config
        :ip => "12.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link5",
        #ip is not applied right now, no auto config
        :ip => "15.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link8",
        #ip is not applied right now, no auto config
        :ip => "18.1.1.1",
        :auto_config => false
    }],
    :ports => [],
    :ram => 4096,
    :provision_script => "bootstrap/iosxr.sh"
  },
  {
    :box => "IOS XRv",
    :hostname => "rtr5",
    :links => [{
        :network => "private_network",
        :vbox_intnet => "mgmt",
        #ip is not applied right now, no auto config
        :ip => "10.1.1.5",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link7",
        #ip is not applied right now, no auto config
        :ip => "17.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link6",
        #ip is not applied right now, no auto config
        :ip => "16.1.1.2",
        :auto_config => false
    },
    {
        :network => "private_network",
        :vbox_intnet => "link8",
        #ip is not applied right now, no auto config
        :ip => "18.1.1.2",
        :auto_config => false
    }],
    :ports => [],
    :ram => 4096,
    :provision_script => "bootstrap/iosxr.sh"
  }
]

# Clients that are not IOS-XRs and a server-like role
servers = [
  {
    :box => "ubuntu/trusty64",
    :hostname => "puppet",
    :links => [{
        :network => "private_network",
        :vbox_intnet => "mgmt",
        :ip => "10.1.1.50",
        :auto_config => true
    }],
    :ports => [],
    :ram => 2048,
    :provision_script => "bootstrap/puppetmaster.sh"
  }
]

# Insert any other sections if necessary. Such as clients.

Vagrant.configure(2) do |config|

    # Provision IOS-XR boxes.
    # Does not have guest additions and thus does not support some options.
    iosxrs.each do |machine|
        config.vm.define machine[:hostname] do |node|
            node.vm.box = machine[:box]
            node.vm.boot_timeout = 600
            node.vm.usable_port_range = (2200..2250)
            # Does not support setting hostname.
            #node.vm.hostname = machine[:hostname]
            # Cannot set IP from Vagrantfile
            machine[:links].each do |link|
                node.vm.network link[:network], virtualbox__intnet: link[:vbox_intnet], auto_config: link[:auto_config]
            end
            machine[:ports].each do |port|
                node.vm.network "forwarded_port", guest: port[:guest], host: port[:host]
            end
            node.ssh.password = "vagrant"
            node.vm.provider "virtualbox" do |vb|
                vb.customize ["modifyvm", :id, "--memory", machine[:ram]]
                vb.name = machine[:hostname]
            end
            # Mounted folders not supported? Need to figure out default way.
            #shares.each do |share|
            #    node.vm.synced_folder share[:host_src], share[:guest_dest]
            #end
            # Copy bootstrap config to router because no shared folders.
            node.vm.provision "file", source: "bootstrap/config/cisco_yang.yaml", destination: "/home/vagrant/cisco_yang.yaml"
            node.vm.provision "file", source: "bootstrap/config/#{machine[:hostname]}", destination: "/home/vagrant/#{machine[:hostname]}"
            node.vm.provision "file", source: "bootstrap/config/demo_agent", destination: "/home/vagrant/"
            node.vm.provision "shell" do |s|
              s.path = machine[:provision_script]
              # Arguments passed in case we want to make a reusable script that generates configs using hostname/ip
              # instead of static config files per device.
              s.args = [machine[:hostname], machine[:links][0][:ip]]
            end
        end
    end

    # Provision servers.
    servers.each do |machine|
        config.vm.define machine[:hostname] do |node|
            node.vm.box = "ubuntu/trusty64"
            node.vm.usable_port_range = (2250..2300)
            node.vm.hostname = machine[:hostname]
            machine[:links].each do |link|
                node.vm.network link[:network], virtualbox__intnet: link[:vbox_intnet], auto_config: link[:auto_config], ip: link[:ip]
            end
            machine[:ports].each do |port|
                node.vm.network "forwarded_port", guest: port[:guest], host: port[:host]
            end
            node.ssh.password = "vagrant"
            node.vm.provider "virtualbox" do |vb|
                vb.customize ["modifyvm", :id, "--memory", machine[:ram]]
                vb.name = machine[:hostname]
            end
            # Add synced folders to guests.
            shares.each do |share|
                node.vm.synced_folder share[:host_src], share[:guest_dest]
            end
            node.vm.provision "shell" do |s|
              s.path = machine[:provision_script]
              s.args = [machine[:hostname]]
            end
        end
    end
end
